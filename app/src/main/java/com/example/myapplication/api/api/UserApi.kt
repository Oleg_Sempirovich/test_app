package com.example.myapplication.api.api

import com.example.myapplication.model.RespDataUser
import retrofit2.http.GET

interface UserApi {

        @GET("users")
        suspend fun getUserData(): RespDataUser
}