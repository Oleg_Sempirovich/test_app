package com.example.myapplication.binding.adapter

import androidx.annotation.ColorInt
import androidx.appcompat.widget.Toolbar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.DataUser
import com.example.myapplication.ui.adapter.BaseDataBindAdapter

@BindingAdapter("setTitle", "setTextColorData")
fun Toolbar.setData(
    textData: String, @ColorInt color: Int
) {
    subtitle = null
    title = textData
    setTitleTextColor(color)
}

@BindingAdapter("setList", "setAdapterBind")
fun RecyclerView.setAdapterBindOrder(
    dataUser: List<DataUser>?,
    adapterOrder: BaseDataBindAdapter<DataUser>?
) {
    if (adapter == null) {
        layoutManager = LinearLayoutManager(context)
        adapter = adapterOrder
    }
    dataUser?.also {
        adapterOrder?.dataList = it
    }

}