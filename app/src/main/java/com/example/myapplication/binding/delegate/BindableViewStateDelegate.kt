package com.example.myapplication.binding.delegate

import com.example.myapplication.viewstate.base.BaseStateView
import kotlin.reflect.KProperty

class BindableViewStateDelegate<in R: BaseStateView,T:Any>(private var value:T, private val bindingEntry:Int) {

    operator fun getValue(thisRef:R,property: KProperty<*>):T = value

    operator fun setValue(thisRef: R, property: KProperty<*>, value: T) {
        this.value = value
        thisRef.notifyPropertyChanged(bindingEntry)
    }
}