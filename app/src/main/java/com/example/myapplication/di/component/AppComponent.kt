package com.example.myapplication.di.component

import com.example.myapplication.App
import com.example.myapplication.di.module.activity.ContributesActivityModule
import com.example.myapplication.di.module.application.AppModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        ContributesActivityModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface AppComponent : AndroidInjector<App?> {

    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<App?>
}
