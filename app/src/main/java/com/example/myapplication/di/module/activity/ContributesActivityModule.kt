package com.example.myapplication.di.module.activity

import com.example.myapplication.di.scope.PerActivity
import com.example.myapplication.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ContributesActivityModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

}
