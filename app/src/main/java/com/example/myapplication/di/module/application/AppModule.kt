package com.example.myapplication.di.module.application

import android.content.Context
import com.example.myapplication.App
import com.example.myapplication.di.provider.RetrofitProvider
import com.example.myapplication.di.scope.context.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @ApplicationContext
    fun provideContext(app: App) = app as Context

    @Singleton
    @Provides
    fun provideRetrofit(@ApplicationContext context: Context) =
        RetrofitProvider(context)

    /**
     * Init local SharedPreference if need
     */
//    @Singleton
//    @Provides
//    fun provideSharedPreference(@ApplicationContext context: Context): SharedPreferenceProvider =
//        SharedPreferenceProvider(context)


    /**
     * Init local DB if need
     */

//    @Singleton
//    @Provides
//    fun provideDB(@ApplicationContext context: Context): AppDB =
//        Room.databaseBuilder(context, AppDB::class.java, AppDB.NAME)
//            .fallbackToDestructiveMigration()
//            .build()
}
