package com.example.myapplication.di.module.fragment

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import com.example.myapplication.R
import com.example.myapplication.api.api.UserApi
import com.example.myapplication.di.provider.RetrofitProvider
import com.example.myapplication.di.scope.context.ApplicationContext
import com.example.myapplication.di.scope.viewmodel.ViewModelKey
import com.example.myapplication.model.DataUser
import com.example.myapplication.ui.adapter.BaseDataBindAdapter
import com.example.myapplication.viewmodel.UserListViewModel
import com.example.myapplication.viewstate.StateViewToolBar
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import androidx.databinding.library.baseAdapters.BR

@Module
abstract class UserListModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserListViewModel::class)
    abstract fun bindViewModel(viewModel: UserListViewModel): ViewModel

    companion object {

        @Provides
        fun provideTextTitle(@ApplicationContext context: Context) =
           StateViewToolBar(context.getText(R.string.app_name).toString(),
               ContextCompat.getColor(context, R.color.colorTextMouthDefault))

        @Provides
        fun provideAdapterDataUser(viewViewModel: UserListViewModel): BaseDataBindAdapter<DataUser> =
            BaseDataBindAdapter(
                R.layout.item_user,
                BR.dataUser,
                viewModel = viewViewModel
            )

        @JvmStatic
        @Provides
        fun provideUserApi(retrofitServiceProvider: RetrofitProvider) =
            retrofitServiceProvider.createService(UserApi::class.java)
    }
}