package com.example.myapplication.di.provider

import com.example.myapplication.api.api.UserApi
import javax.inject.Inject

/**
 * Use providers for fast develop,
 * actually we can divide this approach like Repositories which implements same Interfaces, combine with UseCases or Interactors
 */

class UserProvider @Inject constructor(private val userApi: UserApi) {

    suspend fun getUserListData() = userApi.getUserData().userListData
}