package com.example.myapplication.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class PerFragment
