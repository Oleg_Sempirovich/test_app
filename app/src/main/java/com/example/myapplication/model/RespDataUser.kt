package com.example.myapplication.model

import com.squareup.moshi.Json

data class RespDataUser(@field:Json(name = "data")val userListData:List<DataUser>)