package com.example.myapplication.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentUserListBinding
import com.example.myapplication.model.DataUser
import com.example.myapplication.ui.adapter.BaseDataBindAdapter
import com.example.myapplication.viewmodel.UserListViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class UserListFragment: DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit  var userAdapter: BaseDataBindAdapter<DataUser>

    private val vm by viewModels<UserListViewModel> { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DataBindingUtil.inflate<FragmentUserListBinding>(
            inflater, R.layout.fragment_user_list, container, false).apply {
            partToolBar.toolbar.setupWithNavController(findNavController(),
                AppBarConfiguration(findNavController().graph)
            )
            viewModel = vm
            userDataAdapter = userAdapter
        }.root
    }

}