package com.example.myapplication.viewmodel

import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.viewModelScope
import com.example.myapplication.api.handleerror.ResultWrapper
import com.example.myapplication.api.handleerror.safeApiCall
import com.example.myapplication.di.provider.UserProvider
import com.example.myapplication.model.DataUser
import com.example.myapplication.viewmodel.base.BaseViewModel
import com.example.myapplication.viewstate.StateViewToolBar
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Also we can use alternative approach like MutableLiveData for notify UI,
 * private val _listDataUser = SingleLiveEvent<List<DateUser>>()
   val pinCodeDialogLiveData: LiveData<List<DateUser>> get() = _pinCodeDialogLiveData
 */

class UserListViewModel  @Inject constructor(toolBarStateView: StateViewToolBar,userProvider: UserProvider) : BaseViewModel() {

    @get:Bindable
    val toolBarStateView by bindable(toolBarStateView, BR.toolBarStateView)

    @get:Bindable
    var listUseData by bindable(listOf<DataUser>(), BR.listUseData)

    init {
        viewModelScope.launch {

            val dataSafe = safeApiCall {
                userProvider.getUserListData()
            }
            when (dataSafe) {
                is ResultWrapper.Success -> {
                    listUseData = dataSafe.value
                }
                is ResultWrapper.Error.HttpError -> {
                    /**
                     * logic http error data
                     */
                }
                else -> {
                    /**
                     * logic general error data
                     */
                }
            }

        }
    }

    fun clickOrder(dataUser: DataUser){
        /**
         * logic click
         */
    }
}
