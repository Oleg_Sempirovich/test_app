package com.example.myapplication.viewmodel.base

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import com.example.myapplication.binding.delegate.BindableStateDelegate

open class BaseViewModel : ViewModel(), BindingObservable {

    private val callbacks: PropertyChangeRegistry = PropertyChangeRegistry()

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    override fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    override fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }

    fun <R : BaseViewModel, T : Any?> bindable(value: T, bindingRes: Int): BindableStateDelegate<R, T> =
        BindableStateDelegate(value, bindingRes)

}
