package com.example.myapplication.viewstate

import androidx.annotation.ColorInt
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.example.myapplication.viewstate.base.BaseStateView
import javax.inject.Inject

open class StateViewToolBar @Inject constructor(title:String,
                                                @ColorInt colorTextResId:Int): BaseStateView() {

    @get:Bindable
    var title by bindable(title, BR.title)

    @get:Bindable
    var colorText by bindable(colorTextResId, BR.colorText)

}

