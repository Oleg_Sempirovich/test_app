package com.example.myapplication.viewstate.base

import androidx.databinding.BaseObservable
import com.example.myapplication.binding.delegate.BindableViewStateDelegate

open class BaseStateView: BaseObservable() {
    fun <R: BaseStateView,T:Any> bindable(value:T, bindingRes:Int): BindableViewStateDelegate<R, T> = BindableViewStateDelegate(value,bindingRes)
}